﻿using System;

namespace task19
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Clear();
            //Question A
            var a1 = 6+7;
            var a2 = 3-2;
            var af = a1*a2;
            Console.WriteLine($"Question a = {af}");

            //Question B
            var b1 = 6*7;
            var b2 = 3*2;
            var bf = b1+b2;
            Console.WriteLine($"Question b = {bf}");

            //Question C
            var c1 = 6*7;
            var c2 = 3*2;
            var cf = c1+c2;
            Console.WriteLine($"Question c = {cf}");

            //Question D
            var d1 = 3*2;
            var d2 = 6*7;
            Console.WriteLine($"Question d = {d1+d2}");

            //Question E 
            var e1 = 3*2;
            var e2 = 7*6/2;
            Console.WriteLine($"Question e = {e1+e2}");

            //Question F
            var f1 = 7*3;
            

            //Question K
            Console.WriteLine($"Question K - (6\xB2 + 7) * 3 + 2 = x, where x = {(Math.Pow(6,2)+7)*3+2}");

            Console.ReadKey();
            Console.Clear();
        }
    }
}
